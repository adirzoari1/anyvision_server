
module.exports = {

    development: {
      client: 'pg',
  
      connection: {
        // host : '127.0.0.1',
        host:'localhost',
        port: '5432',
        user : 'postgres',
        password : '123456',
        database : 'AnyVisionTask'
      },
      migrations: {
        tableName: 'migrations'
      },
      seeds: {
        directory: __dirname+'/seeds/'
      },
    },
  
    production: {
      client: 'postgresql',
      connection: process.env.DATABASE_URL
    }
  
  };
  