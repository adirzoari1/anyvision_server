import express from 'express'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import bodyParser from 'body-parser'
import routes from './routes'
import Swagger from './utils/swagger'
// require('dotenv').config()
import swaggerUi from 'swagger-ui-express';

const app = express();



// Middlewares
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit:50000
}));

app.use(cors());
app.use(logger('dev'));

app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    res.send(Swagger)
})
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(Swagger));


app.use('/api',routes)

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handler
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: req.app.get("env") === "development" ? err.stack : {}
    });
});


const port = process.env.PORT || 3014;
app.listen(port,()=>{
    console.log(`Server is listening on port ${port}`)}
)


module.exports = app;
