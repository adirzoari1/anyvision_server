import express from 'express'

import usr from '../controllers/usr'
import stream from '../controllers/stream'
const router = express.Router()

router.use('/health',(req,res)=>{
    res.send('success')
})
router.use('/usr',usr)
router.use('/stream',stream)

export default router;