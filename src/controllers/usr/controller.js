
import UserSrv from '../../srv/usr/user'

class UserCtrl {
    async login(req, res) {
        await UserSrv.login(req.body.email, req.body.password, req.headers.reqid)
          .then(data => {
            return res.json({
              res: true,
              data,
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }

      async register(req, res) {
        await UserSrv.register(req.body.full_name,req.body.email, req.body.password)
          .then(data => {
            return res.json({
              res: true,
              data,
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }
     
}

export default new UserCtrl()
