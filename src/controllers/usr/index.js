import express from 'express'
import UserCtrl from './controller'
import BodyParser from 'body-parser'
import Validate from 'express-validation'
import ParamValidation from '../../utils/paramValidation'

const router = express.Router()

router.route('/login').post([Validate(ParamValidation.login)],UserCtrl.login)
router.route('/register').post([Validate(ParamValidation.register)],UserCtrl.register)


export default router;


 /**
 * @swagger
 * /usr/login:
 *   post:
 *     summary: login user
 *     tags:
 *       - user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         description: login
 *         schema:
 *           type: object
 *           example:
 *              email: test@yopmail.com
 *              password: pass
 *     responses:
 *       '200':
 *         description: User found and logged in successfully
 */

  /**
 * @swagger
 * /usr/register:
 *   post:
 *     summary: register new user
 *     tags:
 *       - user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         description: login
 *         schema:
 *           type: object
 *           example:
 *              full_name: adir zoari
 *              email: test@yopmail.com
 *              password: pass
 *     responses:
 *       '200':
 *         description: register user successful
 */