import express from 'express'
import StreamCtrl from './controller'
import BodyParser from 'body-parser'
import Auth from '../../middlewares/auth/auth';
import {decode} from '../../utils/jwt'
import Validate from 'express-validation'
import ParamValidation from '../../utils/paramValidation'

const router = express.Router()

router.route('/add-stream').post(decode,Auth.checkUser,Auth.decodeUser,[Validate(ParamValidation.add_stream)],StreamCtrl.add)
router.route('/get-streams-by-user-id').get(decode,Auth.checkUser,Auth.decodeUser,StreamCtrl.getStreamsByUserId)




export default router;



  /**
 * @swagger
 * /stream/add-stream:
 *   post:
 *     summary: add stream
 *     tags:
 *       - stream
 *     security:
 *       - JWT: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         required: true
 *         description: login
 *         schema:
 *           type: object
 *           example:
 *              title: stream title
 *              email: rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov
 *             
 *     responses:
 *       '200':
 *         description: Stream inserted successfully
 */

/**
 * @swagger
 * /stream/get-streams-by-user-id:
 *   get:
 *     summary: get all the followers that user follow on them
 *     tags:
 *       - stream
 *     security:
 *       - JWT: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *        
 *     responses:
 *       '200':
 *         description: get streams of user
 */

 