
import StreamSrv from '../../srv/stream/stream'

class StreamCtrl {
    async add(req, res) {
        await StreamSrv.add(req.user.user_id,req.body.title,req.body.url)
          .then(data => {
            return res.json({
              res: true,
              data:data[0]
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }

      async getStreamsByUserId(req, res) {
        await StreamSrv.getStreamsByUserId(req.user.user_id)
          .then(data => {
            return res.json({
              res: true,
              data,
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }

      async getStreamById(req, res) {
        await StreamSrv.getStreamById(req.user.user_id,req.params.stream_id)
          .then(data => {
            if(!data){
              return res.status(400).json({
                res: false,
                msg: 'stream not found'
              })
            }
            return res.json({
              res: true,
              data,
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }
      async watchStream(req, res) {
        await StreamSrv.watchStream(req.body.url)
          .then(data => {
            return res.json({
              res: true,
              data:true
            })
          })
          .catch(err => {
            return res.status(400).json({
              res: false,
              err: err.name,
              code: err.code,
              msg: err.message,
            })
          })
      }
     
     
}

export default new StreamCtrl()
