import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import path from 'path'
const swaggerDefinition = {
    info: {
        title: 'AnyVision FS stack test swagger',
        version: '1.0.0',
        description: 'By Adir Zoari',
    },
    basePath: '/api',
    securityDefinitions: {
        JWT: {
            name: 'Authorization',
            description: 'Must be = "Bearer " + token',
            type: 'apiKey',
            in: 'header',
            
        },
    },
};

const options = {
    swaggerDefinition,
    apis:['./src/controllers/**/*.js']
};

const swaggerSpec = swaggerJSDoc(options);
export default swaggerSpec
