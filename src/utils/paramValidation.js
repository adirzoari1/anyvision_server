import Joi from 'joi';

export default {
  login: {
    body: {
      email: Joi.string().email().required(),
      password: Joi.string().required(),
    },
  },
  register: {
    body: {
      username: Joi.string(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),

    }
  },
  add_stream: {
    body: {
      title: Joi.string().required(),
      url: Joi.string().required(),
    }
  },
  watch_stream: {
    body: {
      url: Joi.string().required(),
    }
  },
 
  

};
