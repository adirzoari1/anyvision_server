import jwt from 'jsonwebtoken'
import { default as jwtDecode } from 'express-jwt'
require('dotenv').config()


class Jwt {

  generate(user_id) {   
  
    console.log('user_id',user_id)
    const token = jwt.sign({ user_id: user_id }, process.env.JWT_TOKEN);
    return `Bearer ${token}`;
  }


}

export default new Jwt();



export const decode = jwtDecode({ secret: process.env.JWT_TOKEN })



