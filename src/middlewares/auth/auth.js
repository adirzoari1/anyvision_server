import jwt from 'express-jwt';
import UserSrv from '../../srv/usr/user'

class Auth {
  async checkUser(err, req, res, next) {
    console.log('user is-->',req.user)

    if (err.name === 'UnauthorizedError') {
        return res.status(401).json('unauthorized')
    }
    if (!req.user) {
        return res.status(401).json('unauthorized')
    }
    next();
  }

  async decodeUser(req, res, next) {
    try {
      const user = await UserSrv.getUserById(req.user.user_id)
      req.user_details = user.data
      next()
    } catch (err) {
      console.log('error',err)
      return res.status(401).json({ err: 1, msg: 'server error', error: err })
    }
  }
}

export default new Auth();
