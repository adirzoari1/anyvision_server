import db from '../../config'

export default class StreamSrv {
    
   
    static async add(user_id,title,url){  
         return await db('streams')
            .insert({
                user_id,
                title,
                url
            }).returning('*')

    }

    static async getStreamsByUserId(user_id){
        return await db('streams')
        .where('user_id',user_id)
        
    }

 


  
}