import Jwt from "../../utils/jwt";
import db from '../../config'
import VError from 'verror'
import BCrypt from 'bcrypt-nodejs'

export default class UserSrv {
    
    static async login(email,password_input){
        let res = await db('users')
        .columns(['full_name','user_id','email', 'password',])
        .first()
        .whereRaw('lower(email) = lower(?)', [email])
        if (!res) {
            throw new VError('No user with %s email found', email)
        }

        let { user_id, password,full_name } = res
        if (!BCrypt.compareSync(password_input, password)) {

          throw new VError('Password does not match')
        }
        const token = Jwt.generate(user_id)
        return {
            token,
            user_id,
            email,
            full_name
        }
    
    }

    static async register(full_name,email,password){
        let res = await db('users')
        .first()
        .whereRaw('lower(email) = lower(?)', [email])
        if(res){
            throw new VError('user with %s email already exists', email)

        }
       let user =  await db('users')
            .insert({
                full_name,
                email,
                password:BCrypt.hashSync(password)
            }).returning(['email','user_id','full_name','created_at'])

        let token = Jwt.generate(user[0].user_id)
        return {
            token,
            email:user[0].email,
            user_id:user[0].user_id,
            full_name:user[0].full_name,
        }
    }
    static async getUserById(user_id){
        let usr = await db('users')
        .where('user_id',user_id)
        if(!usr){
            throw new VError('no user found')
        }
        return usr
       
    }

  
   
}