exports.up = function(knex) {
    return knex.schema.createTable('users', (table) => {
      table.increments('user_id')
      table.string('full_name', 255).notNullable()
      table.string('email', 255).notNullable().unique()
      table.string('password', 60)
      table.timestamp('created_at').defaultTo(knex.fn.now()).notNullable()

    })
  }
  
  exports.down = function(knex) {
    return knex.schema.dropTable('users')
  }
  