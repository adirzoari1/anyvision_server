exports.up = function(knex) {
    return knex.schema.createTable('streams', (table) => {
      table.increments('stream_id')
      table.string('title')
      table.string('url')
      table.integer('user_id').unsigned().references('users.user_id')
      table.timestamp('created_at').defaultTo(knex.fn.now()).notNullable()

    })
  }
  
  exports.down = function(knex) {
    return knex.schema.dropTable('streams')
  }
  